import java.sql.DriverManager
import java.sql.ResultSet

data class Column(val columnName: String, val columnType: String)

fun main(args: Array<String>) {
    val url = "jdbc:firebirdsql://172.26.2.25:3050/c:/tecnopolis/FSElLlano/datos/Spacio/spacio001.gdb"
    val username = "SYSDBA"
    val password = "masterkey"

    val conn = DriverManager.getConnection(url, username, password)
    val meta = conn.metaData
    val tables = meta.getTables(null, null, "%", arrayOf("TABLE"))

    while (tables.next()) {
        val tableName = tables.getString("TABLE_NAME")
        val className = upperCamelCaseConverter(tableName)

        // Get Primary Keys
        val primaryKeys = meta.getPrimaryKeys(null, null, tableName)
        val primaryKeysNames = mutableListOf<String>()

        while (primaryKeys.next()) {
            val column = primaryKeys.getString("COLUMN_NAME")
            primaryKeysNames.add(column)
        }

        // Get Columns
        val tableColumns = meta.getColumns(null, null, tableName, null)
        val tableColumnsList = mutableListOf<Column>()

        while (tableColumns.next()) {
            tableColumnsList.add(
                Column(
                    columnName = tableColumns.getString("COLUMN_NAME"),
                    columnType = tableColumns.getString("TYPE_NAME")
                )
            )
        }

        var pkField: String = ""

        if (primaryKeysNames.size == 1) {
            val pk = tableColumnsList.find { it.columnName == primaryKeysNames[0] }!!

            val columnName = pk.columnName
            val columnType = pk.columnType

            val kotlinType = getHibernateType(columnType)
            val fieldName = camelCaseConverter(columnName)
            pkField += """

                @Id
                @GeneratedValue(strategy = GenerationType.IDENTITY)
                @Column(name = "$columnName")
                val $fieldName: $kotlinType,
            """.trimMargin()
        }

        // Create Entity
        var classString = """
            @Entity
            @Table(name = "$tableName")
            class $className (
                $pkField
        """.trimMargin()

        for (column in tableColumnsList) {

            val columnName = column.columnName.lowercase()
            val fieldName = camelCaseConverter(columnName)

            if (primaryKeysNames.contains(column.columnName)) {
                continue
            }

            val columnType = column.columnType
            val kotlinType = getHibernateType(columnType)

            classString += """
                
                @Column(name = "$columnName")
                val $fieldName: $kotlinType,
            """.trimMargin()
        }

        classString = classString.substring(0, classString.length - 1) + "\n)"

        println(classString)
    }

    conn.close()
}

fun camelCaseConverter(s: String, separator: String = "_"): String {
    val parts = s.split(separator)
    val first = parts[0].lowercase()
    val theRest = parts.subList(1, parts.size).map { part ->
        part.lowercase().replaceFirstChar { it.uppercase() }
    }
    return first + theRest.joinToString("")
}

fun upperCamelCaseConverter(s: String): String {
    val parts = s.lowercase().split("_")
    val result = StringBuilder()
    for (parte in parts) {
        result.append(parte.replaceFirstChar { it.uppercase() })
    }
    return result.toString()
}

fun getHibernateType(sqlTypeName: String): String {
    return when (sqlTypeName) {
        "CHAR", "NCHAR", "VARCHAR", "NVARCHAR", "LONGVARCHAR", "LONGNVARCHAR" -> "String"
        "BIT", "BOOLEAN" -> "Boolean"
        "TINYINT" -> "Byte"
        "SMALLINT" -> "Short"
        "INTEGER" -> "Int"
        "BIGINT" -> "Long"
        "REAL" -> "Float"
        "FLOAT", "DOUBLE" -> "Double"
        "DECIMAL", "NUMERIC" -> "java.math.BigDecimal"
        "DATE" -> "java.time.LocalDate"
        "TIME" -> "java.time.LocalTime"
        "TIMESTAMP" -> "java.time.LocalDateTime"
        "BINARY", "VARBINARY", "LONGVARBINARY" -> "ByteArray"
        "CLOB" -> "java.sql.Clob"
        "BLOB" -> "java.sql.Blob"
        "ARRAY" -> "java.sql.Array"
        "REF" -> "java.sql.Ref"
        "STRUCT" -> "java.sql.Struct"
        else -> "String"
    }
}